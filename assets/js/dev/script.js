//script en ES6

console.log("Hello JS !");

const test = "hello";

console.log(test + " JS !");

//ici j'importe uniquement la const slider
import { slider } from "./classes/slider";
import { tilt } from "./classes/tilt";
import { menu } from "./classes/menu";
import { contact } from "./classes/contact";
import { pagetransition } from "./classes/pagetransition";
